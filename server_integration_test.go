package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestIntegrationHello(t *testing.T) {
	go func() {
		startServer()
	}()

	// Wait for a short period to ensure the server has time to start
	time.Sleep(100 * time.Millisecond)

	req, err := http.NewRequest("GET", "http://localhost:8080/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.DefaultServeMux

	handler.ServeHTTP(rr, req)

	expected := "<h1>Hello Full Cycle!</h1>"
	if rr.Body.String() != expected {
		t.Errorf("Unexpected response. Expected: %s, Got: %s", expected, rr.Body.String())
	}
}
