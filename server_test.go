package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHello(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(hello)

	handler.ServeHTTP(rr, req)

	expected := "<h1>Hello Full Cycle!</h1>"
	if rr.Body.String() != expected {
		t.Errorf("Unexpected response. Expected: %s, Got: %s", expected, rr.Body.String())
	}

	if rr.Code != http.StatusOK {
		t.Errorf("Unexpected status code. Expected: %d, Got: %d", http.StatusOK, rr.Code)
	}
}
